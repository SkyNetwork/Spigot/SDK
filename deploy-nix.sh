#!/usr/bin/env bash

mvn clean deploy -Dmaven.test.skip=true
if [ "$?" -ne 0 ]; then
    echo "Maven Deploy Unsuccessful!"
    exit 1
fi