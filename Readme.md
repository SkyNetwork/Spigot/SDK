### SkyNetwork SDK

[![pipeline status](https://gitlab.com/SkyNetwork/Spigot/SDK/badges/master/pipeline.svg)](https://gitlab.com/SkyNetwork/Spigot/SDK/commits/master) 
[![coverage report](https://gitlab.com/SkyNetwork/Spigot/SDK/badges/master/coverage.svg)](https://gitlab.com/SkyNetwork/Spigot/SDK/commits/master)

This plugin provides a simple software development kit to developers working on plugins for SkyNetwork. There are some
essential core features here that all developers working on SkyNetwork need to utilize during plugin development. One
such feature is the Database connection class within the SDK. The only way to interact with the database on our server
is by using the SDK's SQL class in [Database\SQL.java](https://gitlab.com/SkyNetwork/Spigot/SDK/blob/master/src/main/java/com/skynetwork/sdk/Database/SQL.java)

Generated Documentation: [doc.lawlsec.org](https://doc.lawlsec.org/SDK/)

##### Importing with Maven
```xml
<repositories>
    <!-- Other Repositories -->

    <repository>
        <id>snapshots</id>
        <!-- Note that this url may change as we push closer to production! -->
        <url>http://maven.shellcode.ml/content/repositories/snapshots</url>
    </repository>
</repositories>

<dependencies>
    <!-- Other Dependencies -->

    <dependency>
        <groupId>com.skynetwork.sdk</groupId>
        <artifactId>SDK</artifactId>
        <version>LATEST</version>
    </dependency>
</dependencies>
```

The [wiki](https://gitlab.com/SkyNetwork/Spigot/SDK/wikis/home) contains examples and detailed descriptions of some key components in SDK, for example [using SDK from Maven](https://gitlab.com/SkyNetwork/Spigot/SDK/wikis/Using-SDK-from-Maven) and [using the Database connection](https://gitlab.com/SkyNetwork/Spigot/SDK/wikis/Using-the-Database)