### Code Style

Code contributions should be well documented, clear and concise contributions. It is important for SDK to be easily readable by future developers and people that are using the plugin in their projects. We may reject contributions based on their readability.

Code contributions should take advantage of modern Java features and not use legacy code unless required. We also encourage contributors to modernize the codebase if they spot legacy code in the project.

SDK is intended to be used with the latest versions of Spigot/Bukkit only, this means contributors should avoid using parts of the Spigot or Bukkit projects that make updating versions more difficult; for example the NMS classes. Ideally SDK should be able to switch Spigot/Bukkit versions with no changes to the code base.