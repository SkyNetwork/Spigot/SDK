package com.skynetwork.sdk;

import com.skynetwork.sdk.Database.Accounts.AccountLevelController;
import com.skynetwork.sdk.Database.Accounts.MemberPreferenceController;
import com.skynetwork.sdk.Database.Accounts.OnlineMemberController;
import com.skynetwork.sdk.Database.Chat.ChatChannelController;
import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Database.Debugger.DebugLogController;
import com.skynetwork.sdk.Database.Notifications.NotificationController;
import com.skynetwork.sdk.Database.SQL;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Listeners.ChatListeners;
import com.skynetwork.sdk.Listeners.DebugListener;
import com.skynetwork.sdk.Listeners.PlayerEventListeners;
import com.skynetwork.sdk.Objects.Chat.Channels.LocalChannel;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class SDK extends JavaPlugin {
    private static SDK Instance;
    private static List<Controller> Controllers;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Instance = this;

        ///region Listeners

        getServer().getPluginManager().registerEvents(new PlayerEventListeners(), this);
        getServer().getPluginManager().registerEvents(new ChatListeners(), this);
        getServer().getPluginManager().registerEvents(new DebugListener(), this);

        ///endregion

        ///region Controllers

        Controllers = new ArrayList<>();
        addController(new AccountLevelController());
        addController(new MemberPreferenceController());
        addController(new OnlineMemberController());
        addController(new ChatChannelController());
        addController(new NotificationController());
        addController(new DebugLogController());

        ///endregion

        ChatChannelController chatController =
            (ChatChannelController)getController(ChatChannelController.class);

        if (chatController != null) {
            chatController.registerChannel(new LocalChannel(), true);
        }
    }

    public static Controller getController(Class type) {
        for (Controller controller : Controllers) {
            if (type.isInstance(controller))
                return controller;

        }

        return null;
    }

    /**
     * Add a controller to the SDK's controller list
     * This method will automatically start the controller
     * @param controller Controller to add and start
     */
    public static void addController(Controller controller) {
        if (Controllers.contains(controller))
            return;

        new Logit(Instance).Log(
            String.format(
                "Starting Controller: %s", controller.getClass().getName()

            ), Logit.Level.Info
        );

        controller.init();
        controller.start();
        Controllers.add(controller);
    }

    /**
     * Removes a controller from the SDK's controller list
     * This method will automatically stop the controller
     * @param controller Controller to stop and remove
     */
    public static void removeController(Controller controller) {
        if (!Controllers.contains(controller))
            return;

        controller.stop();
        Controllers.remove(controller);
    }

    @Override
    public void onDisable() {
        Controllers.forEach(Controller::stop);
    }

    /**
     * Get static instance of this plugin
     * @return Static SDK instance
     */
    public static SDK getInstance() {
        return Instance;
    }
}
