package com.skynetwork.sdk.Exceptions;

public class ChannelNotRegisteredException extends RuntimeException {
    private String Message;

    public ChannelNotRegisteredException(String Message) {
        this.Message = Message;
    }

    public String getMessage() {
        return Message;
    }
}
