package com.skynetwork.sdk.Exceptions;

public class IncompleteDatabaseConfigException extends RuntimeException {
    private String Message;

    public IncompleteDatabaseConfigException(String Message) {
        this.Message = Message;
    }

    public String getMessage() {
        return Message;
    }
}
