package com.skynetwork.sdk.Exceptions;

public class ChannelAlreadyRegisteredException extends RuntimeException {
    private String Message;

    public ChannelAlreadyRegisteredException(String Message) {
        this.Message = Message;
    }

    public String getMessage() {
        return Message;
    }
}
