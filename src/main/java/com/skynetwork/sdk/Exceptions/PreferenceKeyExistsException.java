package com.skynetwork.sdk.Exceptions;

public class PreferenceKeyExistsException extends RuntimeException {
    private String Message;

    public PreferenceKeyExistsException(String Message) {
        this.Message = Message;
    }

    public String getMessage() {
        return Message;
    }
}
