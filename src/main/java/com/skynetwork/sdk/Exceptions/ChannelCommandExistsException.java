package com.skynetwork.sdk.Exceptions;

public class ChannelCommandExistsException extends RuntimeException {
    private String Message;

    public ChannelCommandExistsException(String Message) {
        this.Message = Message;
    }

    public String getMessage() {
        return Message;
    }
}
