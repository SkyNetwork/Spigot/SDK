package com.skynetwork.sdk.Listeners;

import com.skynetwork.sdk.Database.Debugger.DebugLogController;
import com.skynetwork.sdk.Events.DebugLogEvent;
import com.skynetwork.sdk.SDK;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class DebugListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onDebugLog(DebugLogEvent e) {
        new BukkitRunnable() {
            @Override
            public void run() {
                DebugLogController controller =
                    ((DebugLogController) SDK.getController(DebugLogController.class));

                if (controller != null) {
                    controller.addLog(e.getCaller(), e.getMessage(), e.getLevel(), e.getTrace());
                }
            }

        }.runTaskAsynchronously(SDK.getInstance());
    }
}
