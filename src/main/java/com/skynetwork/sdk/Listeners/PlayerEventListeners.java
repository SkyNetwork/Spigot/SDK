package com.skynetwork.sdk.Listeners;

import com.skynetwork.sdk.Database.Accounts.OnlineMemberController;
import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Events.MemberEvents.MemberFirstJoinEvent;
import com.skynetwork.sdk.Events.MemberEvents.MemberJoinEvent;
import com.skynetwork.sdk.Events.MemberEvents.MemberQuitEvent;
import com.skynetwork.sdk.SDK;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerEventListeners implements Listener {
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerJoin(PlayerJoinEvent e) {
        new Logit(SDK.getInstance()).Log(
            String.format("Player %s joined with UUID: %s",
                e.getPlayer().getName(),
                e.getPlayer().getUniqueId().toString()

            ), Logit.Level.Info
        );

        Controller memberController = SDK.getController(OnlineMemberController.class);

        if (memberController != null) {
            ((OnlineMemberController)memberController).startSession(e.getPlayer());

        } else {
            new Logit(SDK.getInstance()).Log(
                "memberController is null!",
                Logit.Level.Error
            );

            return;
        }

        if (((OnlineMemberController)memberController).isFirstPlay(e.getPlayer().getUniqueId())) {
            Bukkit.getServer().getPluginManager().callEvent(
                new MemberFirstJoinEvent(
                    ((OnlineMemberController)memberController).getMember(e.getPlayer().getUniqueId())
                )
            );
        }

        Bukkit.getServer().getPluginManager().callEvent(
            new MemberJoinEvent(
                ((OnlineMemberController)memberController)
                    .getOnlineMembers()
                    .get(e.getPlayer().getUniqueId()))

        );
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerQuit(PlayerQuitEvent e) {
        new Logit(SDK.getInstance()).Log(
            String.format("Player %s quit",
                e.getPlayer().getName()

            ), Logit.Level.Info
        );

        Controller memberController = SDK.getController(OnlineMemberController.class);

        Bukkit.getServer().getPluginManager().callEvent(
            new MemberQuitEvent(
                ((OnlineMemberController)memberController)
                    .getOnlineMembers()
                    .get(e.getPlayer().getUniqueId()))

        );

        ((OnlineMemberController)memberController).quitSession(e.getPlayer());
    }
}
