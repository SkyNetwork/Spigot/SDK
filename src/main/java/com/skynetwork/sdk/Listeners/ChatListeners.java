package com.skynetwork.sdk.Listeners;

import com.skynetwork.sdk.Database.Accounts.OnlineMemberController;
import com.skynetwork.sdk.Database.Chat.ChatChannelController;
import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Objects.Chat.Channel;
import com.skynetwork.sdk.Objects.Chat.Message;
import com.skynetwork.sdk.Objects.Member.Member;
import com.skynetwork.sdk.SDK;
import com.skynetwork.sdk.Utilities.General;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class ChatListeners implements Listener {
    @EventHandler
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent e) {

        // If this message does not contain a space or a / it's not a proper command
        if (!e.getMessage().contains(" ") || !e.getMessage().startsWith("/")) {
            e.setCancelled(false);
            return;
        }

        String message = e.getMessage().substring(e.getMessage().indexOf(" "));
        String command = e.getMessage().substring(0, e.getMessage().indexOf(" "));

        try {
            Controller memberController = SDK.getController(OnlineMemberController.class);
            Controller chatController = SDK.getController(ChatChannelController.class);

            Member sender = null;

            if (memberController == null) {
                e.setCancelled(false);
                return;
            }

            sender = ((OnlineMemberController)memberController).getOnlineMember(e.getPlayer());

            Channel channel = null;

            if (chatController == null) {
                e.setCancelled(false);
                return;
            }

            channel = ((ChatChannelController)chatController).getChannelByCommand(command);

            if (channel != null) {
                e.setCancelled(true);

                channel.receiveMessage(new Message(
                    sender, e.getMessage(), message, General.getCurrentTimestamp()
                ));

            } else {
                e.setCancelled(false);

                new Logit(SDK.getInstance()).Log(
                    String.format("Cannot find channel for %s", command),
                    Logit.Level.Error
                );
            }

        } catch (NullPointerException ex) {
            new Logit(SDK.getInstance()).Log(
                "NullPointer in CommandPreprocessEvent",
                Logit.Level.Error,
                ex.getStackTrace()
            );

            e.setCancelled(false);
        }
    }
}
