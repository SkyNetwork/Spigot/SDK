package com.skynetwork.sdk.Debugger;

import com.skynetwork.sdk.Config.LoggerConfig;
import com.skynetwork.sdk.Events.DebugLogEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

public class Logit {
    private Plugin Caller;
    private LoggerConfig Config;

    /**
     * Log Level
     */
    public enum Level {
        Info, Warning, Error, Critical, Other
    }

    /**
     * Automatically disables the plugin on critical error
     */
    private void CriticalPlugin() {
        Log(String.format(
            "Encountered critical error from \"%s\"! Shutting down plugin..",
            Caller.getName()

        ), Level.Error);

        Bukkit.getPluginManager().disablePlugin(Caller);
    }
    
    private void CriticalServer() {
        Log(String.format(
            "Encountered critical error from \"%s\"! Shutting down server..",
            Caller.getName()
            
        ), Level.Error);
        
        Bukkit.getServer().shutdown();
    }

    /**
     * Get number representation of log level
     * @param level Level to get number representation for
     * @return Number representing log level
     */
    private static int getLevelInt(Level level) {
        switch (level) {
            case Info: return 1;
            case Warning: return 2;
            case Error: return 3;
            default: return 5;
        }
    }
    
    /**
     * Translate a log Level into a ChatColor
     * @param LogLevel Level to translate
     * @return ChatColor equivalent
     */
    private ChatColor getColor(Level LogLevel) {
        switch(LogLevel) {
            case Info: return ChatColor.WHITE;
            case Warning: return ChatColor.YELLOW;
            case Error: return ChatColor.RED;
            case Critical: return ChatColor.DARK_RED;
            default: return ChatColor.GRAY;
        }
    }

    /**
     * Log a message to the debug logger
     * @param Message Message to log
     */
    public void Log(String Message) {
        Log(Message, Level.Other, null);
    }

    /**
     * Log a message to the debug logger
     * @param Message Message to log
     * @param LogLevel Level of message
     */
    public void Log(String Message, Level LogLevel) {
        Log(Message, Level.Other, null);
    }

    /**
     * Log a message to the debug logger
     * @param Message Message to log
     * @param LogLevel Level of message
     * @param Trace StackTraceElement from Exception
     */
    public void Log(String Message, Level LogLevel, StackTraceElement[] Trace) {
        /*
         * Check if the level set in the configuration is more than
         * the level of the current log message, if so print to the
         * console. This is intended to control the amount of log
         * messages printed to console in order to keep log file size
         * in check. Critical messages always show in console.
         */
        if ((LogLevel == Level.Critical) || (getLevelInt(LogLevel) <= getLevelInt(Config.getPrintLevel()))) {
            
            /*
             * Format the message for easy reading
             * then dispatch the message to Console
             */
            Bukkit.getServer().getConsoleSender().sendMessage(
                String.format("[%s%s%s]: %s%s",
                    ChatColor.AQUA, Caller.getName(), ChatColor.RESET,
                    getColor(LogLevel), Message
            ));
        }

        /*
         * Call the event so that listeners will
         * be notified that a plugin has invoked
         * the Debug Logger
         */
        Bukkit.getServer().getPluginManager().callEvent(
            new DebugLogEvent(Caller, Message, LogLevel, Trace)
        );
        
        /*
         * If log level is critical, process critical error
         * functions in order. Server Critical shutdown has
         * to be processed before plugin Critical shutdown
         */
        if (LogLevel == Level.Critical) {
        
            /*
             * If config is set to shutdown the server on Critical
             * error run the function to shutdown the server
             */
            if (Config.getCriticalServer()) {
                CriticalServer();
                return;
            }
        
            /*
             * If config is set to shutdown the plugin on Critical
             * error run the function to shutdown the plugin
             */
            if (Config.getCriticalPlugin()) {
                CriticalPlugin();
            }
        }
    }

    /**
     * Debug Logger class, provide instance of your
     * plugin so that the logger can tag things properly.
     * @param Caller Plugin instance
     */
    public Logit(Plugin Caller) {
        this.Caller = Caller;
        this.Config = new LoggerConfig();
    }
}
