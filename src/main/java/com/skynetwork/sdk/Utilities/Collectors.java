package com.skynetwork.sdk.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class Collectors {
    public static <T> Collector<T, List<T>, T> singletonCollector() {
        return Collector.of(ArrayList::new, List::add, (left, right) -> { left.addAll(right); return left; },
            list -> {
                if (list.size() != 1)
                    return null;

                return list.get(0);
            }
        );
    }
}
