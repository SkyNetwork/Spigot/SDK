package com.skynetwork.sdk.Utilities.Messages;

import org.bukkit.ChatColor;
import org.bukkit.util.ChatPaginator;

import java.util.ArrayList;
import java.util.List;

public class MessageDefaults {
    
    /**
     * Chat message types
     */
    public enum MessageType { Info, Prompt, Warning, Error, Special, Prefix }

    /**
     * Returns the proper color for the global
     * color scheme
     * @param Type Type of message
     * @return Color for the type of messasge
     */
    public static ChatColor getSchemeColor(MessageType Type) {
        switch (Type) {
            case Info: return ChatColor.WHITE;
            case Prompt: return ChatColor.AQUA;
            case Warning: return ChatColor.RED;
            case Error: return ChatColor.DARK_RED;
            case Special: return ChatColor.GOLD;
            case Prefix: return ChatColor.BLUE;
            default: return ChatColor.WHITE;
        }
    }

    /**
     * Formats a prefix to the correct format
     * for the global text style
     * @param Input String to make into prefix
     * @return Properly formatted prefix
     */
    public static String createPrefix(String Input) {
        return String.format("[%s%s%s]",
            getSchemeColor(MessageType.Prefix),
            Input, ChatColor.RESET
        );
    }

    /**
     * Creates a properly formatted, split and prefixed chat message
     * @param Type Type of message (for coloring)
     * @param Prefix Message prefix
     * @param Message Message content
     * @return String list of messages
     */
    @Deprecated
    public static List<String> formatChatMessage(MessageType Type, String Prefix, String Message) {
        List<String> retVal = new ArrayList<>();
        String MessagePrefix = createPrefix(Prefix);
        String[] WrappedMessage = ChatPaginator.wordWrap(
            Message, ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH - MessagePrefix.length()
        );

        for (String MessageStr : WrappedMessage) {
            retVal.add(String.format("%s %s%s",
                MessagePrefix, getSchemeColor(Type), MessageStr)
            );
        }

        return retVal;
    }

    /**
     * Splits a long message into shorter messages
     * @param message Message to split
     * @return Split message
     */
    public static String[] wordWrap(String message) {
        return ChatPaginator.wordWrap(message, ChatPaginator.GUARANTEED_NO_WRAP_CHAT_PAGE_WIDTH);
    }
}
