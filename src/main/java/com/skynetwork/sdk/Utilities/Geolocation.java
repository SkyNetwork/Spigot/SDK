package com.skynetwork.sdk.Utilities;

import com.maxmind.geoip.*;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.SDK;

import java.io.IOException;

public class Geolocation {
    /**
     * Location class to be used for
     * geolocation methods
     */
    private Location location;

    /**
     * Geolocation instance for a single address
     * @param address IP Address of client
     */
    public Geolocation(String address) {
        Logit logger = new Logit(SDK.getInstance());
        logger.Log(
            String.format(
                "Geolocation Database: %s",
                SDK.getInstance().getDataFolder() + "/GeoLiteCity.dat"

            ), Logit.Level.Info
        );

        try {
            LookupService service = new LookupService(
                SDK.getInstance().getDataFolder() + "/GeoLiteCity.dat",
                LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE
            );

            location = service.getLocation(address);

        } catch (IOException e) {
            logger.Log(
                String.format(
                    "Cannot find Geolocation Database: %s",
                    SDK.getInstance().getDataFolder() + "/GeoLiteCity.dat"

                ), Logit.Level.Warning
            );
        }
    }

    /**
     * Gets the client time zone
     * @return client time zone
     */
    public String getTimezone() {
        return timeZone.timeZoneByCountryAndRegion(location.countryCode, location.region);
    }

    /**
     * Gets the client country
     * @return client country
     */
    public String getCountryName() {
        return location.countryName;
    }

    /**
     * Gets the client region
     * @return client region
     */
    public String getRegionName() {
        return location.region;
    }

    /**
     * Gets the client city
     * @return client city
     */
    public String getCityName() {
        return location.city;
    }
}
