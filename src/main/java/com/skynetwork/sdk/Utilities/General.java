package com.skynetwork.sdk.Utilities;

import java.net.InetSocketAddress;
import java.sql.Timestamp;

public class General {
    /**
     * Gets a Timestamp object representing the current system time
     * @return Timestamp object representing the current system time
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }
    
    /**
     * Check if the user is a Llama
     * @return Boolean value of the user's current Llama status
     */
    public static boolean isUserLlama() {
        return true;
    }
    
    /**
     * Checks a string and returns a plural value (*s) if number is not 1 or -1
     * Probably not needed but easier to have here, also not possessive aware
     * @param number Number of item(s)
     * @param input String representing item(s)
     * @return String with proper pluralization
     */
    public static String fixPlural(int number, String input) {
        return (number == 1) || number == -1 ? input : String.format("%ss", number);
    }

    /**
     * Gets a string formatted IP Address from an InetSocketAddress object
     * @param address InetSocketAddress object to parse
     * @return String formatted IP Address
     */
    public static String getIPAddress(InetSocketAddress address) {
        return address.getAddress().toString().split("/")[1];
    }
}
