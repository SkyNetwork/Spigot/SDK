package com.skynetwork.sdk.Objects.Chat.Sockets;

import com.skynetwork.sdk.Objects.Member.Member;

import java.net.Socket;

public class AuthenticatedSocket extends Socket {
    private Member member;

    public Member getMember() {
        return member;
    }

    public AuthenticatedSocket(Member member) {
        this.member = member;
    }
}
