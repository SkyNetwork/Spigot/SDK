package com.skynetwork.sdk.Objects.Chat;

import com.skynetwork.sdk.Objects.Chat.Sockets.AuthenticatedSocket;

public interface Channel {
    String getName();
    String getCommand();
    void receiveMessage(Message message);
    void socketConnected(AuthenticatedSocket socket);
    void socketDisconnected(AuthenticatedSocket socket);
}
