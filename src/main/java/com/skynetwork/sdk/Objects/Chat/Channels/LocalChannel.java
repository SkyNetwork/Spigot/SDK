package com.skynetwork.sdk.Objects.Chat.Channels;

import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Objects.Chat.Sockets.AuthenticatedSocket;
import com.skynetwork.sdk.Objects.Chat.Channel;
import com.skynetwork.sdk.Objects.Chat.Message;
import com.skynetwork.sdk.SDK;
import com.skynetwork.sdk.Utilities.Messages.MessageDefaults;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class LocalChannel implements Channel {
    private Logit Logger;
    private ServerSocket serverSocket;
    private List<Socket> clientSockets;

    @Override
    public String getName() {
        return "Local";
    }

    @Override
    public String getCommand() {
        return "/l";
    }

    @Override
    public void receiveMessage(Message message) {

        // Just log to console since it's for testing
        new Logit(SDK.getInstance()).Log(
            MessageDefaults.createPrefix(getName()) + " " +
            message.getSender().getPlayer().getDisplayName() + ":" +
            message.getFormattedMessage()
        );

        // Send chat to island members @BlendyCat
    }

    @Override
    public void socketConnected(AuthenticatedSocket socket) {
        if (socket == null)
            return;

        throw new NotImplementedException();
    }

    @Override
    public void socketDisconnected(AuthenticatedSocket socket) {
        if (socket == null)
            return;

        throw new NotImplementedException();
    }

    public LocalChannel() {
        Logger = new Logit(SDK.getInstance());

        try {
            serverSocket = new ServerSocket();

        } catch (IOException e) {
            serverSocket = null;

            Logger.Log(String.format(
                "IOException while opening server socket for %s", getName()

            ), Logit.Level.Error);
        }
    }
}
