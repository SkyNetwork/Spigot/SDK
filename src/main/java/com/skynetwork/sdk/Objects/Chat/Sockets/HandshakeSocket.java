package com.skynetwork.sdk.Objects.Chat.Sockets;

import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Objects.Member.Member;
import com.skynetwork.sdk.SDK;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class HandshakeSocket {
    private Logit Logger;
    private ServerSocket serverSocket;
    private ConcurrentHashMap<Member, Socket> clientSockets;
    private Boolean closed = false;

    public HandshakeSocket() {
        Logger = new Logit(SDK.getInstance());
        clientSockets = new ConcurrentHashMap<>();

        try {
            serverSocket = new ServerSocket(25500);

        } catch (IOException e) {
            serverSocket = null;

            Logger.Log(
                "IOException while opening server socket for %s",
                Logit.Level.Error, e.getStackTrace()
            );
        }
    }

    public void listen() {
        Bukkit.getScheduler().runTaskAsynchronously(SDK.getInstance(), () -> {
            while (!closed) {

            }
        });
    }
}
