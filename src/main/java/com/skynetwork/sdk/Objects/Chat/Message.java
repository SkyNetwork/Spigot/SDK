package com.skynetwork.sdk.Objects.Chat;

import com.skynetwork.sdk.Objects.Member.Member;

import java.sql.Timestamp;

public class Message {
    private Member sender;
    private String preFormattedMessage;
    private String formattedMessage;
    private Timestamp timestamp;

    public Member getSender() {
        return sender;
    }

    public void setSender(Member sender) {
        this.sender = sender;
    }

    public String getPreFormattedMessage() {
        return preFormattedMessage;
    }

    public void setPreFormattedMessage(String preFormattedMessage) {
        this.preFormattedMessage = preFormattedMessage;
    }

    public String getFormattedMessage() {
        return formattedMessage;
    }

    public void setFormattedMessage(String formattedMessage) {
        this.formattedMessage = formattedMessage;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Message(Member sender, String preFormattedMessage, String formattedMessage, Timestamp timestamp) {
        this.sender = sender;
        this.preFormattedMessage = preFormattedMessage;
        this.formattedMessage = formattedMessage;
        this.timestamp = timestamp;
    }
}
