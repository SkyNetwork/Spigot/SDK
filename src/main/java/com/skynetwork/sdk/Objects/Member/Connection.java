package com.skynetwork.sdk.Objects.Member;

public class Connection {
    private String address;
    private String timezone;
    private String country;
    private String subdivision;
    private String city;

    public String getAddress() {
        return address;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getCountry() {
        return country;
    }

    public String getSubdivision() {
        return subdivision;
    }

    public String getCity() {
        return city;
    }

    public Connection(String address, String timezone, String country, String subdivision, String city) {
        this.address = address;
        this.timezone = timezone;
        this.country = country;
        this.subdivision = subdivision;
        this.city = city;
    }
}
