package com.skynetwork.sdk.Objects.Member;

import com.skynetwork.sdk.Database.Accounts.MemberPreferenceController;
import com.skynetwork.sdk.Exceptions.PreferenceKeyExistsException;
import com.skynetwork.sdk.SDK;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class Preferences {
    private Plugin caller;
    private Player player;
    private HashMap<String, String> prefStrings;

    public String getString(String key) {
        if (prefStrings.containsKey(key))
            return prefStrings.get(key);

        return null;
    }

    public void putString(String key, String value) {
        if (prefStrings.containsKey(key))
            throw new PreferenceKeyExistsException(String.format("Preference with key \"%s\" already exists!", key));

        prefStrings.put(key, value);
    }

    public void deletePreference(String key) {
        if (prefStrings.containsKey(key))
            prefStrings.remove(key);

    }

    public void savePreferences() {
        MemberPreferenceController preferenceController = (MemberPreferenceController)SDK.getController(MemberPreferenceController.class);
        preferenceController.putStringPrefsMap(caller, player, prefStrings);
    }

    public Preferences(Plugin caller, Player player) {
        MemberPreferenceController preferenceController = (MemberPreferenceController)SDK.getController(MemberPreferenceController.class);

        this.caller = caller;
        this.player = player;
        this.prefStrings = preferenceController.getStringPrefsMap(caller, player);
    }
}
