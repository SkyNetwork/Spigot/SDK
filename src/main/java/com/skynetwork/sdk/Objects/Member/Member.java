package com.skynetwork.sdk.Objects.Member;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class Member {
    private Player player;
    private Session session;
    private Connection connection;
    private HashMap<String, Preferences> preferences;

    public Player getPlayer() {
        return player;
    }

    public Session getSession() {
        return session;
    }

    public Connection getConnection() {
        return connection;
    }

    public Preferences getPreferences(Plugin plugin) {
        if (preferences.containsKey(plugin.getName()))
            return preferences.get(plugin.getName());

        return null;
    }

    public void addPreferences(Plugin plugin, Preferences prefs) {
        if (!preferences.containsKey(plugin.getName()))
            preferences.put(plugin.getName(), prefs);
    }

    public void savePreferences() {
        if ((preferences != null) && (!preferences.isEmpty())) {
            preferences.forEach((key, value) -> {
                value.savePreferences();
            });
        }
    }

    public Member(Player player, Session session, Connection connection) {
        this.player = player;
        this.session = session;
        this.connection = connection;
        this.preferences = new HashMap<>();
    }
}
