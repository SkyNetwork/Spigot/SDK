package com.skynetwork.sdk.Objects.Member;

import java.sql.Timestamp;

public class Session {
    private Timestamp start;
    private Timestamp end;
    private boolean first;

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public boolean isFirst() {
        return first;
    }

    public Session(Timestamp start) {
        this.start = start;
        this.end = end;
    }

    public Session(Timestamp start, boolean first) {
        this.start = start;
        this.end = end;
        this.first = first;
    }

    public Session(Timestamp start, Timestamp end) {
        this.start = start;
        this.end = end;
    }
}
