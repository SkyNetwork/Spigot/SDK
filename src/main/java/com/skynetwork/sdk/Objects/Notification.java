package com.skynetwork.sdk.Objects;

import com.skynetwork.sdk.Objects.Member.Member;

public class Notification {
    private Member sender;
    private Member receiver;
    private String tag;
    private String message;

    public Member getSender() {
        return sender;
    }

    public Member getReceiver() {
        return receiver;
    }

    public String getTag() {
        return tag;
    }

    public String getMessage() {
        return message;
    }

    public Notification(Member sender, Member receiver, String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public Notification(Member sender, Member receiver, String tag, String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.tag = tag;
        this.message = message;
    }
}
