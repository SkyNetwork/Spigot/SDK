package com.skynetwork.sdk.Events.MemberEvents;

import com.skynetwork.sdk.Objects.Member.Member;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MemberFirstJoinEvent extends Event {
    private Member member;

    private static final HandlerList Handlers = new HandlerList();

    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }

    public Member getMember() {
        return member;
    }

    public MemberFirstJoinEvent(Member member) {
        this.member = member;
    }
}
