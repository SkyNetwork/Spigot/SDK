package com.skynetwork.sdk.Events;

import com.skynetwork.sdk.Objects.Notification;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NotificationSentEvent extends Event {
    private Notification notification;

    private static final HandlerList Handlers = new HandlerList();

    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }

    public Notification getNotification() {
        return notification;
    }

    public NotificationSentEvent(Notification notification) {
        this.notification = notification;
    }
}
