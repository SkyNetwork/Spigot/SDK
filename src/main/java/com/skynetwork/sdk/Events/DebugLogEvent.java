package com.skynetwork.sdk.Events;

import com.skynetwork.sdk.Debugger.Logit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

public class DebugLogEvent extends Event {
    private Plugin Caller;
    private String Message;
    private Logit.Level Level;
    private StackTraceElement[] Trace;

    private static final HandlerList Handlers = new HandlerList();

    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }

    public Plugin getCaller() {
        return Caller;
    }

    public String getMessage() {
        return Message;
    }

    public Logit.Level getLevel() {
        return Level;
    }

    public StackTraceElement[] getTrace() {
        return Trace;
    }

    public DebugLogEvent(Plugin Caller, String Message) {
        this.Caller = Caller;
        this.Message = Message;
    }

    public DebugLogEvent(Plugin Caller, String Message, Logit.Level Level) {
        this.Caller = Caller;
        this.Message = Message;
        this.Level = Level;
    }

    public DebugLogEvent(Plugin Caller, String Message, Logit.Level Level, StackTraceElement[] Trace) {
        this.Caller = Caller;
        this.Message = Message;
        this.Level = Level;
        this.Trace = Trace;
    }
}
