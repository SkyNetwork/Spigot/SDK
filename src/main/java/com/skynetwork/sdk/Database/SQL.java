package com.skynetwork.sdk.Database;

import com.skynetwork.sdk.Config.DatabaseConfig;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Exceptions.IncompleteDatabaseConfigException;
import com.skynetwork.sdk.SDK;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.plugin.Plugin;

import java.sql.*;
import java.util.HashMap;
import java.util.Properties;

public class SQL {

    /**
     * Global instance of the Hikari Data Source
     */
    private static HikariDataSource databaseSource = null;

    /**
     * A HashMap responsible for keeping track of the
     * number of connections a plugin has open
     */
    private static HashMap<Plugin, Integer> connectionTracker = new HashMap<>();

    /**
     * Initializes the database, should only be called once on
     * plugin startup
     *
     * @return Boolean representing if the init was successful
     * @throws Exception If database connection was not successful
     */
    public static boolean init() throws Exception {
        if (null == databaseSource) {
            databaseSource = new HikariDataSource(getDBConfig());

            try {

                databaseSource.getConnection().close();

                return true;

            } catch (Exception e) {

                databaseSource = null;

                new Logit(SDK.getInstance()).Log(
                    "Database configuration is invalid. Check config.yml",
                    Logit.Level.Critical
                );

                return false;
            }

        } else {
            return true;
        }
    }

    /**
     * Creates Hikari config from database config section in
     * the SDK resources
     *
     * @return Database configuration object for Hikari
     */
    private static HikariConfig getDBConfig() {
        DatabaseConfig _Config = new DatabaseConfig();

        return getProps(
            _Config.getHost(),
            Integer.valueOf(_Config.getPort()),
            _Config.getDatabase(),
            _Config.getUser(),
            _Config.getPass()
        );
    }

    /**
     * Sets up a Hikari config with provided variables
     *
     * @param host Database host
     * @param port Database port (Default: 3306)
     * @param database Database name
     * @param username Database username
     * @param password Database password
     * @return Database configuration object for Hikari
     */
    private static HikariConfig getProps(String host, int port, String database, String username, String password) {
        Properties props = new Properties();

        props.setProperty("jdbcUrl", String.format("jdbc:mysql://%s:%d/%s?autoReconnect=true&useSSL=false", host, port, database));
        props.setProperty("username", username);
        props.setProperty("password", password);
        props.setProperty("dataSource.cachePrepStmts", "true");
        props.setProperty("dataSource.prepStmtCacheSize", "250");
        props.setProperty("dataSource.prepStmtCacheSqlLimit", "2048");
        props.setProperty("dataSource.useServerPrepStmts", "true");
        props.setProperty("dataSource.useLocalSessionState", "true");
        props.setProperty("dataSource.useLocalTransactionState", "true");
        props.setProperty("dataSource.rewriteBatchedStatements", "true");
        props.setProperty("dataSource.cacheResultSetMetadata", "true");
        props.setProperty("dataSource.cacheServerConfiguration", "true");
        props.setProperty("dataSource.elideSetAutoCommits", "true");
        props.setProperty("dataSource.maintainTimeStats", "false");

        return new HikariConfig(props);
    }

    /**
     * Gets a Connection instance from the Hikari connection pool
     * for plugins to use to interact with the database. Make sure
     * that you close this connection!
     *
     * @param Caller Plugin that is requesting a database connection
     * @return A connection instance from Hikari's connection pool
     * @throws SQLException If connection could not be obtained
     */
    public static Connection getPoolConnection(Plugin Caller) throws SQLException {
        Logit Logger = new Logit(SDK.getInstance());
        DatabaseConfig _Config = new DatabaseConfig();

        Logger.Log(String.format("Creating database connection for %s",
            Caller.getName()), Logit.Level.Info
        );

        if (!_Config.isValid()) {
            Logger.Log("Database configuration is invalid. Check config.yml",
                Logit.Level.Error
            );

            throw new IncompleteDatabaseConfigException(
                "SDK cannot create a database connection with an incomplete configuration."
            );
        }

        try {
            SQL.init();

            if (connectionTracker.containsKey(Caller)) {
                connectionTracker.put(Caller,
                    connectionTracker.get(Caller) + 1
                );

            } else connectionTracker.put(Caller, 1);

        } catch (Exception e) {
            Logger.Log("Failed to obtain pooled connection to database SQL.init()",
                Logit.Level.Error
            );
        }

        return databaseSource.getConnection();
    }

    /**
     * See {@link #getPoolConnection(Plugin)} this method should only be used
     * for plugins that cannot use Logit for whatever reason.
     *
     * @param Caller Plugin that is requesting a database connection
     * @return A connection instance from Hikari's connection pool
     * @throws SQLException If connection could not be obtained
     */
    public static Connection getPoolConnection_NoLogger(Plugin Caller) throws SQLException {
        DatabaseConfig _Config = new DatabaseConfig();

        if (!_Config.isValid()) {
            throw new IncompleteDatabaseConfigException(
                "SDK cannot create a database connection with an incomplete configuration."
            );
        }

        try {
            SQL.init();

            if (connectionTracker.containsKey(Caller)) {
                connectionTracker.put(Caller,
                    connectionTracker.get(Caller) + 1
                );

            } else connectionTracker.put(Caller, 1);

        } catch (Exception ignored) { }

        return databaseSource.getConnection();
    }

    /**
     * Attempts to close a supplied connection properly
     *
     * @param plugin Plugin calling to close connection
     * @param connection Connection to close
     */
    public static void close(Plugin plugin, Connection connection) {
        close(plugin, null, null, connection);
    }

    /**
     * Attempts to close a supplied connection and prepared
     * statement properly
     *
     * @param plugin Plugin calling to close connection
     * @param statement Statement to close
     * @param connection Connection to close
     */
    public static void close(Plugin plugin, Statement statement, Connection connection) {
        close(plugin, null, statement, connection);
    }

    /**
     * Attempts to close a supplied connection and result set properly
     *
     * @param plugin Plugin calling to close connection
     * @param resultSet Result set to close
     * @param connection Connection to close
     */
    public static void close(Plugin plugin, ResultSet resultSet, Connection connection) {
        close(plugin, resultSet, null, connection);
    }

    /**
     * Attempts to close a supplied connection, statement and
     * result set properly
     *
     * @param plugin Plugin calling to close connection
     * @param resultSet Result set to close
     * @param statement Statement to close
     * @param connection Connection to close
     */
    public static void close(Plugin plugin, ResultSet resultSet, Statement statement, Connection connection) {
        if (connection != null) {
            try {
                connection.close();

                if (connectionTracker.containsKey(plugin)) {
                    int conCount = connectionTracker.get(plugin);

                    if ((conCount - 1) <= 0) {
                        connectionTracker.remove(plugin);

                    } else connectionTracker.put(plugin, conCount - 1);

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (statement != null) {
            try {
                statement.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (resultSet != null) {
            try {
                resultSet.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get a connection to the database.
     * It is your job to close this connection!
     *
     * @deprecated  depreciated direct access to the database in favor of Hikari pooled connections<br>
     *              <strong>Will be removed in next version!</strong><br>
     *              use {@link #getPoolConnection(Plugin)} instead like this:
     *
     * <blockquote>
     * <pre>
     * Connection conn = SQL.getHikariConnection(pluginInstance);
     * </pre>
     * </blockquote>
     *
     * @throws SQLException Connection could not be obtained
     * @throws ClassNotFoundException JBDC could not be found
     *
     * @param Caller Plugin that is requesting a database connection
     * @return Connection to Database
     */
    public static Connection getConnection(Plugin Caller) throws SQLException, ClassNotFoundException {
        Logit Logger = new Logit(SDK.getInstance());
        DatabaseConfig _Config = new DatabaseConfig();

        Logger.Log(String.format("Creating database connection for %s",
            Caller.getName()), Logit.Level.Info
        );

        if (!_Config.isValid()) {
            Logger.Log("Database configuration is invalid. Check config.yml",
                Logit.Level.Error
            );

            throw new IncompleteDatabaseConfigException(
                "SDK cannot create a database connection with an incomplete configuration."
            );
        }

        Class.forName("com.mysql.jdbc.Driver");

        Logger.Log(String.format("Created database connection for %s",
            Caller.getName()), Logit.Level.Info
        );

        return DriverManager.getConnection(
            String.format(
                "jdbc:mysql://%s:%s/%s?useSSL=true",
                _Config.getHost(),
                _Config.getPort(),
                _Config.getDatabase()
            ),
            _Config.getUser(),
            _Config.getPass()
        );
    }

    /**
     * Get a connection to the database.
     * It is your job to close this connection!
     * This method should be avoided unless the
     * logger causes issues with your code for
     * some reason!
     *
     * @deprecated  depreciated direct access to the database in favor of Hikari pooled connections<br>
     *              <strong>Will be removed in next version!</strong><br>
     *              use {@link #getPoolConnection_NoLogger(Plugin)} instead like this:
     *
     * <blockquote>
     * <pre>
     * Connection conn = SQL.getHikariConnection_NoLogger(pluginInstance);
     * </pre>
     * </blockquote>
     *
     * @throws SQLException Connection could not be obtained
     * @throws ClassNotFoundException JBDC could not be found
     *
     * @param Caller Plugin that is requesting a database connection
     * @return Connection to Database
     */
    public static Connection getConnection_NoLogger(Plugin Caller) throws SQLException, ClassNotFoundException {
        DatabaseConfig _Config = new DatabaseConfig();

        if (!_Config.isValid()) {
            throw new IncompleteDatabaseConfigException(
                "SDK cannot create a database connection with an incomplete configuration."
            );
        }

        Class.forName("com.mysql.jdbc.Driver");

        return DriverManager.getConnection(
            String.format(
                "jdbc:mysql://%s:%s/%s?useSSL=true",
                _Config.getHost(),
                _Config.getPort(),
                _Config.getDatabase()
            ),
            _Config.getUser(),
            _Config.getPass()
        );
    }
}
