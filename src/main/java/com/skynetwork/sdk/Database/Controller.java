package com.skynetwork.sdk.Database;

public interface Controller {
    void init();
    void start();
    void stop();
}
