package com.skynetwork.sdk.Database.Debugger;

import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Database.SQL;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.SDK;
import org.bukkit.plugin.Plugin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DebugLogController implements Controller {
    private Logit Logger;

    @Override
    public void init() {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection_NoLogger(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS `SDK_Logs` (" +
                            "`caller` VARCHAR(45)," +
                            "`message` VARCHAR(255)," +
                            "`level` VARCHAR(30)," +
                            "`file` VARCHAR(255)," +
                            "`class` VARCHAR(255)," +
                            "`method` VARCHAR(255)," +
                            "`line` INT(5)," +
                            "`exception` VARCHAR(255)," +
                            "`time` TIMESTAMP DEFAULT NOW()" +
                        ");"
            );

            Statement.executeUpdate();

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException ignored) {
            // nothing

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    @Override
    public void start() {
        Logger.Log(
            String.format(
                "Received start() signal on %s", this.getClass().getName()

            ), Logit.Level.Info
        );
    }

    @Override
    public void stop() { }

    public void addLog(Plugin caller, String message, Logit.Level logLevel, StackTraceElement[] trace) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection_NoLogger(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "INSERT INTO SDK_Logs SET `caller`=?, `message`=?, `level`=?, `file`=?, `class`=?, `method`=?, `line`=?, `exception`=?;"
            );

            String ex_File = "Unknown";
            String ex_Class = "Unknown";
            String ex_Method = "Unknown";
            String ex_Exception = "Unknown";
            int ex_Line = 0;

            if ((trace != null) && (trace[0] != null)) {
                ex_File = trace[0].getFileName();
                ex_Class = trace[0].getClassName();
                ex_Method = trace[0].getMethodName();
                ex_Exception = trace[0].toString();
                ex_Line = trace[0].getLineNumber();
            }

            // if the message is longer than 255 chars just use the last 255 chars
            if (ex_Exception.length() > 255)
                ex_Exception = ex_Exception.substring(ex_Exception.length() - 255);

            Statement.setString(1, caller.getName());
            Statement.setString(2, message);
            Statement.setString(3, logLevel.name());
            Statement.setString(4, ex_File);
            Statement.setString(5, ex_Class);
            Statement.setString(6, ex_Method);
            Statement.setInt(7, ex_Line);
            Statement.setString(8, ex_Exception);
            Statement.executeUpdate();

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException ignored) {
            // nothing

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * The debug log controller handles database logging
     * for the Logit debugger
     */
    public DebugLogController() {
        Logger = new Logit(SDK.getInstance());
    }
}
