package com.skynetwork.sdk.Database.Notifications;

import com.skynetwork.sdk.Database.Accounts.OnlineMemberController;
import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Database.Debugger.DebugLogController;
import com.skynetwork.sdk.Database.SQL;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Events.NotificationSentEvent;
import com.skynetwork.sdk.Objects.Member.Member;
import com.skynetwork.sdk.Objects.Notification;
import com.skynetwork.sdk.SDK;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class NotificationController implements Controller {
    private Logit Logger;

    @Override
    public void init() {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS `SDK_Notifications` (" +
                            "`sender` VARCHAR(36)," +
                            "`receiver` VARCHAR(36)," +
                            "`tag` VARCHAR(36)," +
                            "`message` VARCHAR(255)," +
                            "`read` BOOL DEFAULT FALSE" +
                        ");"
            );

            Statement.executeUpdate();

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Send a notification to a player, this does not check if
     * the player is online it simply sends dispatches the
     * notification along the notification bus.
     *
     * @param notification Notification to send
     */
    public void sendNotification(Notification notification) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "INSERT INTO SDK_Notifications SET `sender`=?, `receiver`=?, `tag`=?, `message`=?;"
            );

            Statement.setString(1, notification.getSender().getPlayer().getUniqueId().toString());
            Statement.setString(2, notification.getReceiver().getPlayer().getUniqueId().toString());
            Statement.setString(3, notification.getTag());
            Statement.setString(4, notification.getMessage());
            Statement.executeUpdate();

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }

        Bukkit.getServer().getPluginManager().callEvent(
            new NotificationSentEvent(notification)
        );
    }

    /**
     * Gets unread notifications that have been sent to a player.
     * You must mark notifications read individually with {@link #markRead(Notification)}
     *
     * @param receiver Player to get notifications for
     * @return List of unread notifications for the specified player
     */
    public List<Notification> getNotifications(Member receiver) {
        List<Notification> notifications = new ArrayList<>();

        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Notifications WHERE `receiver`=? AND `read`=FALSE;"
            );

            Statement.setString(1, receiver.getPlayer().getUniqueId().toString());

            ResultSet result = Statement.executeQuery();

            Controller memberController = SDK.getController(OnlineMemberController.class);

            while (result.next()) {
                Member sender = null;

                if (memberController != null) {
                    sender = ((OnlineMemberController)memberController).getMember(
                        UUID.fromString(
                            result.getString("sender")
                        )
                    );
                }

                notifications.add(new Notification(
                    sender,
                    receiver,
                    result.getString("tag"),
                    result.getString("message")
                ));
            }

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }

        return notifications;
    }

    /**
     * Marks a single notification as read. Notifications must be
     * displayed to the user before being marked as read.
     *
     * @param notification Notification to mark as read
     */
    public void markRead(Notification notification) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Connection conn = null;
                PreparedStatement Statement = null;

                try {
                    conn = SQL.getPoolConnection(SDK.getInstance());
                    Statement = conn.prepareStatement(
                            "UPDATE SDK_Notifications SET `read`=TRUE WHERE `sender`=? AND `receiver`=? AND `tag`=? AND `message`=?;"
                    );

                    Statement.setString(1, notification.getSender().getPlayer().getUniqueId().toString());
                    Statement.setString(2, notification.getReceiver().getPlayer().getUniqueId().toString());
                    Statement.setString(3, notification.getTag());
                    Statement.setString(4, notification.getMessage());
                    Statement.executeUpdate();

                    SQL.close(SDK.getInstance(), conn);

                } catch (SQLException e) {
                    Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
                    Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

                } finally { SQL.close(SDK.getInstance(), Statement, conn); }
            }

        }.runTaskAsynchronously(SDK.getInstance());
    }

    @Override
    public void start() {
        Logger.Log(
            String.format(
                "Received start() signal on %s", this.getClass().getName()

            ), Logit.Level.Info
        );
    }

    @Override
    public void stop() { }

    /**
     * The notification controller provides a way to store
     * and retrieve tagged messages to and from players.
     * The intended purpose of this controller as the name
     * implies is to provide a notification system for certain
     * chat channels that require one.
     */
    public NotificationController() {
        Logger = new Logit(SDK.getInstance());
    }
}
