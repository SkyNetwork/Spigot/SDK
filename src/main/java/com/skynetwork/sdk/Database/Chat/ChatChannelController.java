package com.skynetwork.sdk.Database.Chat;

import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Exceptions.ChannelAlreadyRegisteredException;
import com.skynetwork.sdk.Exceptions.ChannelCommandExistsException;
import com.skynetwork.sdk.Exceptions.ChannelNotRegisteredException;
import com.skynetwork.sdk.Objects.Chat.Channel;
import com.skynetwork.sdk.Objects.Member.Member;
import com.skynetwork.sdk.SDK;
import com.skynetwork.sdk.Utilities.Collectors;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class ChatChannelController implements Controller {
    private Logit Logger;

    public void init() { }

    /**
     * List of registered channels to lighten the
     * load on the database.
     */
    private HashMap<Channel, Boolean> RegisteredChannels = new HashMap<>();

    /**
     * Get a registered Channel by its command
     * @param command Command to look for
     * @return Registered channel with searched command
     */
    public Channel getChannelByCommand(String command) {
        Logger.Log(
            String.format("Looking for Channel with %s registered ..", command),
            Logit.Level.Info
        );

        if (RegisteredChannels.isEmpty())
            return null;

        return RegisteredChannels.keySet().stream()
            .filter((channel) -> channel.getCommand().equals(command))
            .collect(Collectors.singletonCollector());
    }

    /**
     * Checks if a command is registered with a channel
     * @param command Command to look for
     * @return boolean value indicating if the command is registered with a channel
     */
    private boolean channelCommandExists(String command) {
        Logger.Log(
            String.format("Checking if command %s exists ..", command),
            Logit.Level.Info
        );

        if (RegisteredChannels.isEmpty())
            return false;

        return RegisteredChannels.keySet()
            .stream()
            .anyMatch(
                (channel) -> channel.getCommand().equals(command));

    }

    /**
     * Starts a registered channel. If the channel
     * is not already registered this method will
     * register the channel for you before starting.
     * @param channel Channel to start
     */
    public void startChannel(Channel channel) {
        Logger.Log(
            String.format("Starting channel %s with command %s ..", channel.getName(), channel.getCommand()),
            Logit.Level.Info
        );

        /*
         * If the channel needs to be registered
         * just register the channel with autoStart
         * set to true.
         */
        if (!RegisteredChannels.keySet().contains(channel)) {
            Logger.Log(
                String.format("Channel %s is not already registered, registering instead ..", channel.getName()),
                Logit.Level.Info
            );

            registerChannel(channel, true);
            return;
        }

        /*
         * If the value is trye the channel is
         * already started so there is no need
         * to stop the channel.
         */
        if (RegisteredChannels.get(channel))
            return;

        /*
         * Start a channel by replacing its enabled
         * value within the HashMap.
         */
        RegisteredChannels.replace(channel, true);

        Logger.Log(
            String.format(
                "Channel %s enabled: %s ..",
                channel.getName(),
                RegisteredChannels.get(channel)
            ), Logit.Level.Info
        );
    }

    /**
     * Register a chat channel with its handler
     * @param channel Channel to register
     * @param autoStart If true the channel will start upon registration
     */
    public void registerChannel(Channel channel, boolean autoStart) {
        if (RegisteredChannels.keySet().contains(channel)) {
            throw new ChannelAlreadyRegisteredException(
                String.format("Channel %s already exists!", channel.getName())
            );
        }

        if (channelCommandExists(channel.getCommand())) {
            throw new ChannelCommandExistsException(
                String.format("A channel with the command %s already exists!", channel.getCommand())
            );
        }

        RegisteredChannels.put(channel, autoStart);
    }

    /**
     * Stops a registered channel. The channel must be
     * registered or the method will throw an exception.
     * @param channel Channel to stop
     */
    public void stopChannel(Channel channel) {
        Logger.Log(
            String.format("Stopping channel %s with command %s ..", channel.getName(), channel.getCommand()),
            Logit.Level.Info
        );

        if (!RegisteredChannels.keySet().contains(channel)) {
            throw new ChannelNotRegisteredException(
                String.format("Channel %s is not registered!", channel.getName())
            );
        }

        /*
         * If the value is false the channel is
         * already stopped so there is no need
         * to stop the channel.
         */
        if (!RegisteredChannels.get(channel))
            return;

        RegisteredChannels.replace(channel, false);

        Logger.Log(
            String.format(
                "Channel %s enabled: %s ..",
                channel.getName(),
                RegisteredChannels.get(channel)
            ), Logit.Level.Info
        );
    }

    /**
     * Deregister a chat channel
     * Do not deregister channels that you didn't register!
     * @param channel Channel to deregister
     */
    public void deregisterChannel(Channel channel) {
        if (!RegisteredChannels.keySet().contains(channel))
            throw new ChannelNotRegisteredException(
                String.format("Channel %s is not registered!", channel.getName())
            );

        RegisteredChannels.remove(channel);
    }

    /**
     * Loads chat channels registered in the database
     * into memory for faster access. This must be
     * called only once, on the start of the plugin.
     */
    public void start() {

    }

    /**
     * Shuts down all chat channels and saves data
     * to the database. This must be called only once,
     * on quit of the plugin.
     */
    public void stop() {
        Iterator<Map.Entry<Channel, Boolean>> entryIt = RegisteredChannels.entrySet().iterator();

        while (entryIt.hasNext()) {
            Map.Entry<Channel, Boolean> entry = entryIt.next();
            stopChannel(entry.getKey());
            entryIt.remove();
        }
    }

    /**
     * Controller for system wide chat channels. This
     * allows the SDK to have more internal control over
     * the chat system.
     */
    public ChatChannelController() {
        Logger = new Logit(SDK.getInstance());
    }
}
