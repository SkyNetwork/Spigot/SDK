package com.skynetwork.sdk.Database.Accounts;

import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Database.SQL;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.Objects.Member.Member;
import com.skynetwork.sdk.Objects.Member.Session;
import com.skynetwork.sdk.SDK;
import com.skynetwork.sdk.Utilities.General;
import com.skynetwork.sdk.Utilities.Geolocation;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.sql.Connection;

public class OnlineMemberController implements Controller {
    private Logit Logger;
    private HashMap<UUID, Member> OnlineMembers;

    @Override
    public void init() {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS `SDK_Sessions` (" +
                            "`uuid` VARCHAR(36)," +
                            "`address` VARCHAR(255)," +
                            "`timezone` VARCHAR(255)," +
                            "`country` VARCHAR(255)," +
                            "`subdivision` VARCHAR(255)," +
                            "`city` VARCHAR(255)," +
                            "`timeIn` TIMESTAMP NULL DEFAULT NULL," +
                            "`timeOut` TIMESTAMP NULL DEFAULT NULL" +
                        ");"
            );

            Statement.executeUpdate();

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Get Member objects for each member that is online.
     * Used for lookups and things like that.
     * @return HashMap of UUID's and Member objects
     */
    public HashMap<UUID, Member> getOnlineMembers() {
        return OnlineMembers;
    }

    /**
     * Lookup an online Member from a Player object
     * @param player Player object to look up
     * @return Online Member
     */
    public Member getOnlineMember(Player player) {

        // ToDo: Refactor to more "modern" loop

        for (Map.Entry<UUID, Member> entry : OnlineMembers.entrySet()) {
            if (entry.getKey().equals(player.getUniqueId()))
                return entry.getValue();

        }

        return null;
    }

    /**
     * Gets a Member object for a specified UUID. If the player is
     * offline this returns a less useful member object not containing
     * any session or preferences data.
     * @param uuid UUID to create Member object for
     * @return Member object for provided UUID
     */
    public Member getMember(UUID uuid) {
        Player player =
                (Bukkit.getPlayer(uuid) != null) ? Bukkit.getPlayer(uuid) : Bukkit.getOfflinePlayer(uuid).getPlayer();

        Member member = getOnlineMember(player);

        if (member != null) {
            return member;
        }

        return new Member(player, null, null);
    }

    /**
     * Checks if there are any previous sessions for the supplied
     * player's UUID. If not that would indicate this is the
     * first time the player has logged in.
     * @param uuid UUID of Player to lookup
     * @return Boolean representing if this is the first session for the user
     */
    public boolean isFirstPlay(UUID uuid) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                "SELECT * FROM SDK_Sessions WHERE `uuid`=? AND `timeOut` IS NOT NULL LIMIT 1;"
            );

            Statement.setString(1, uuid.toString());

            boolean result = Statement.executeQuery().next();

            SQL.close(SDK.getInstance(), conn);

            return result;

        } catch (SQLException e) {
            Logger.Log("Cannot read member sessions from database: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);
            return false;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Writes an entire Member class to the database, this
     * function is primarily used for in memory caching.
     * @param member Member class to save
     */
    private void commitData(Member member) {
        Logger = new Logit(SDK.getInstance());

        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                "INSERT INTO SDK_Sessions SET `uuid`=?, `address`=?, `timezone`=?, `country`=?, `subdivision`=?, `city`=?, `timeIn`=?, `timeOut`=?;"
            );

            Statement.setString(1, member.getPlayer().getUniqueId().toString());
            Statement.setString(2, member.getConnection().getAddress());
            Statement.setString(3, member.getConnection().getTimezone());
            Statement.setString(4, member.getConnection().getCountry());
            Statement.setString(5, member.getConnection().getSubdivision());
            Statement.setString(6, member.getConnection().getCity());
            Statement.setTimestamp(7, member.getSession().getStart());
            Statement.setTimestamp(8, member.getSession().getEnd());
            Statement.executeUpdate();

            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Cannot commit member data to database: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Starts a cached session for the specified player, this
     * should be called each time a player logs in to the server
     * BUT make sure that #quitSession() is called to save and
     * remove the opened session from in memory cache.
     * @param player Player to load cached session for
     */
    public void startSession(Player player) {
        Geolocation geoLocation = new Geolocation(
            General.getIPAddress(player.getAddress())
        );

        Session session = new Session(
            new Timestamp(System.currentTimeMillis()),
            isFirstPlay(player.getUniqueId())
        );

        OnlineMembers.put(player.getUniqueId(), new Member(
            player, session, new com.skynetwork.sdk.Objects.Member.Connection(
                player.getAddress().toString(),
                (geoLocation.getTimezone() != null) ? geoLocation.getTimezone() : "Unknown",
                (geoLocation.getCountryName() != null) ? geoLocation.getCountryName() : "Unknown",
                (geoLocation.getRegionName() != null) ? geoLocation.getRegionName() : "Unknown",
                (geoLocation.getCityName() != null) ? geoLocation.getCityName() : "Unknown"
            )
        ));
    }

    /**
     * Quits a cached session for the specified player, this
     * should be called each time a player logs out of the server.
     * If the session could not be found in cache it will just
     * return immediately to prevent unforseen issues.
     * @param player Player to quit cached session for
     */
    public void quitSession(Player player) {
        if (OnlineMembers.containsKey(player.getUniqueId())) {
            new Logit(SDK.getInstance()).Log(
                String.format("Found session for %s, ending session..",
                    player.getName()

                ), Logit.Level.Info
            );

            Member member = OnlineMembers.get(player.getUniqueId());
            member.getSession().setEnd(new Timestamp(System.currentTimeMillis()));
            member.savePreferences();

            new BukkitRunnable() {
                @Override
                public void run() { commitData(member); }

            }.runTaskAsynchronously(SDK.getInstance());

            OnlineMembers.remove(player.getUniqueId());

        } else {
            new Logit(SDK.getInstance()).Log(
                String.format("Cannot find existing session for %s!",
                    player.getName()

                ), Logit.Level.Error
            );
        }
    }

    @Override
    public void start() {
        Logger.Log(
            String.format(
                "Received start() signal on %s", this.getClass().getName()

            ), Logit.Level.Info
        );

        OnlineMembers = new HashMap<>();
        Bukkit.getOnlinePlayers().forEach(this::startSession);
    }

    /**
     * Fully stops the entire OnlineMemberController.
     * WARNING: This method is only made for use
     *          when the server is stopped. Using
     *          this method otherwise will probably
     *          cause serious issues throughout the
     *          server and its plugins!
     */
    @Override
    public void stop() {
        Iterator<Map.Entry<UUID, Member>> entryIt = OnlineMembers.entrySet().iterator();

        while (entryIt.hasNext()) {
            Map.Entry<UUID, Member> entry = entryIt.next();

            commitData(entry.getValue());
            entryIt.remove();
        }
    }

    /**
     * The online member controller keeps track of online players
     * on any platform via the shared database. This allows plugins
     * to access information about players regardless of platform
     */
    public OnlineMemberController() {
        Logger = new Logit(SDK.getInstance());
    }
}
