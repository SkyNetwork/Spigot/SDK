package com.skynetwork.sdk.Database.Accounts;

import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Database.SQL;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.SDK;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import javax.swing.plaf.nimbus.State;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AccountLevelController implements Controller {
    private Logit Logger;

    public enum AccountLevel {
        Unconfirmed,
        Player,
        Media,
        Moderator,
        Developer,
        Administrator
    }

    @Override
    public void init() {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());

            Statement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS `SDK_Members` (" +
                            "`id` INT(6) PRIMARY KEY," +
                            "`uuid` VARCHAR(36)," +
                            "`level` VARCHAR(25)," +
                            "`linked` BOOL DEFAULT FALSE" +
                        ");" //id, uuid, level
            );

            Statement.executeUpdate();
            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Checks if an account with the given ID exists
     * in the database table
     * @param id User ID
     * @return Value indicating if account exists
     */
    public boolean isAccount(String id) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Members WHERE `id`=?;"
            );

            Statement.setString(1, id);

            boolean ret = Statement.executeQuery().next();

            SQL.close(SDK.getInstance(), conn);

            return ret;

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            return false;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Checks if an account with the given UUID exists
     * in the database table
     * @param Member Player to check for (with UUID)
     * @return Value indicating if account exists
     */
    public boolean isAccount(Player Member) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Members WHERE `uuid`=?;"
            );

            Statement.setString(1, Member.getUniqueId().toString());

            boolean ret = Statement.executeQuery().next();

            SQL.close(SDK.getInstance(), conn);

            return ret;

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            return false;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Gets account level for player with supplied ID
     * @param id ID of player to check
     * @return Account Level of player
     */
    public AccountLevel getAccountLevel(String id) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Members WHERE `id`=?;"
            );

            Statement.setString(1, id);

            AccountLevel ret = AccountLevel.valueOf(Statement.executeQuery().getString("level"));

            SQL.close(SDK.getInstance(), conn);

            return ret;

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            return null;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Gets account level for player with supplied ID
     * @param Member Player to check
     * @return Account Level of player
     */
    public AccountLevel getAccountLevel(Player Member) {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Members WHERE `uuid`=?;"
            );

            Statement.setString(1, Member.getUniqueId().toString());

            AccountLevel ret = AccountLevel.valueOf(Statement.executeQuery().getString("level"));

            SQL.close(SDK.getInstance(), conn);

            return ret;

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            return null;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Update account level for a player. Do NOT use this.
     * @param id ID of account to modify
     * @param Member Player to modify
     * @param Level Account level to set
     */
    public void updateAccountLevel(String id, Player Member, AccountLevel Level) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Connection conn = null;
                PreparedStatement Statement = null;

                try {
                    conn = SQL.getPoolConnection(SDK.getInstance());
                    Statement = conn.prepareStatement(
                            "UPDATE SDK_Members SET `uuid`=?, `level`=? WHERE `id`=?;"
                    );

                    Statement.setString(1, Member.getUniqueId().toString());
                    Statement.setString(2, Level.name());
                    Statement.setString(3, id);
                    Statement.executeUpdate();

                    SQL.close(SDK.getInstance(), conn);

                } catch (SQLException e) {
                    Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);

                } finally { SQL.close(SDK.getInstance(), Statement, conn); }
            }

        }.runTaskAsynchronously(SDK.getInstance());
    }

    /**
     * Update account level for a player. Do NOT use this.
     * @param Member Player to modify
     * @param Level Account level to set
     */
    public void updateAccountLevel(Player Member, AccountLevel Level) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Connection conn = null;
                PreparedStatement Statement = null;

                try {
                    conn = SQL.getPoolConnection(SDK.getInstance());
                    Statement = conn.prepareStatement(
                            "UPDATE SDK_Members SET `level`=? WHERE `uuid`=?;"
                    );

                    Statement.setString(1, Level.name());
                    Statement.setString(2, Member.getUniqueId().toString());
                    Statement.executeUpdate();

                    SQL.close(SDK.getInstance(), conn);

                } catch (SQLException e) {
                    Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);

                } finally { SQL.close(SDK.getInstance(), Statement, conn); }
            }

        }.runTaskAsynchronously(SDK.getInstance());
    }

    @Override
    public void start() {
        Logger.Log(
            String.format(
                "Received start() signal on %s", this.getClass().getName()

            ), Logit.Level.Info
        );
    }

    @Override
    public void stop() { }

    /**
     * The account level controller provides an easy way to
     * set or modify user account levels and provides a layer
     * of private account data between the web ui and the game.
     * This controller should not be used outside of SDK unless
     * absolutely required.
     */
    public AccountLevelController() {
        Logger = new Logit(SDK.getInstance());
    }
}
