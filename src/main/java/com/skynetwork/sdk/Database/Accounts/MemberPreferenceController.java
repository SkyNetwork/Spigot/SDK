package com.skynetwork.sdk.Database.Accounts;

import com.skynetwork.sdk.Database.Controller;
import com.skynetwork.sdk.Database.SQL;
import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.SDK;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class MemberPreferenceController implements Controller {
    private Logit Logger;

    @Override
    public void init() {
        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS `SDK_Preferences` (" +
                            "`plugin` VARCHAR(255)," +
                            "`uuid` VARCHAR(36)," +
                            "`pref` VARCHAR(255)," +
                            "`val` VARCHAR(255)" +
                        ");"
            );

            Statement.executeUpdate();
            SQL.close(SDK.getInstance(), conn);

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            Logger.Log("SQLException: " + e.getMessage(), Logit.Level.Error);

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Gets a HashMap with key value pairs for the specified player and plugin
     * @param caller Plugin that is looking for its player's preferences
     * @param player Player that the plugin needs to fetch the preferences for
     * @return HashMap of player preferences from the database
     */
    public HashMap<String, String> getStringPrefsMap(Plugin caller, Player player) {
        HashMap<String, String> retVal = new HashMap<>();

        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Preferences WHERE `plugin`=? AND `uuid`=?;"
            );

            Statement.setString(1, caller.getName());
            Statement.setString(2, player.getUniqueId().toString());

            ResultSet Result = Statement.executeQuery();

            while (Result.next()) {
                retVal.put(
                    Result.getString("key"),
                    Result.getString("value")
                );
            }

            SQL.close(SDK.getInstance(), conn);

            return retVal;

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            return null;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Saves a HashMap with key value pairs for the specified player and plugin
     * @param caller Plugin that is saving the player's preferences
     * @param player Player that the plugin is saving the preferences for
     * @param prefsMap HashMap of preferences for the player
     */
    public void putStringPrefsMap(Plugin caller, Player player, HashMap<String, String> prefsMap) {
        prefsMap.forEach((key, value) -> {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (getStringPref(caller, player, key) != null) {
                        Logger.Log(
                            String.format(
                                "Found key \"%s\", updating value..", key
                            ), Logit.Level.Info
                        );

                        updStringPref(caller, player, key, value);

                    } else {
                        Logger.Log(
                            String.format(
                                "Did not find key \"%s\", inserting key with value \"%s\"..", key, value
                            ), Logit.Level.Info
                        );

                        putStringPref(caller, player, key, value);
                    }
                }

            }.runTaskAsynchronously(SDK.getInstance());
        });
    }

    /**
     * Looks for s specific string preference
     * @param caller Plugin that is looking for the specific string preference
     * @param player Player that the preference would be saved for
     * @param key Key of the preference that is saved in the database
     * @return Value of the specified preference if it exists, otherwise null
     */
    private String getStringPref(Plugin caller, Player player, String key) {
        String retVal;

        Connection conn = null;
        PreparedStatement Statement = null;

        try {
            conn = SQL.getPoolConnection(SDK.getInstance());
            Statement = conn.prepareStatement(
                    "SELECT * FROM SDK_Preferences WHERE `plugin`=? AND `uuid`=? AND `pref`=?;"
            );

            Statement.setString(1, caller.getName());
            Statement.setString(2, player.getUniqueId().toString());
            Statement.setString(3, key);

            ResultSet Result = Statement.executeQuery();

            retVal = Result.getString("value");

            SQL.close(SDK.getInstance(), conn);

            return retVal;

        } catch (SQLException e) {
            Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);
            return null;

        } finally { SQL.close(SDK.getInstance(), Statement, conn); }
    }

    /**
     * Uses INSERT to insert new preferences into the database
     * @param caller Plugin that is inserting the preference
     * @param player Player that the plugin is inserting the preference for
     * @param key Key of the preference to be inserted
     * @param value Value of the preference that is being inserted
     */
    private void putStringPref(Plugin caller, Player player, String key, String value) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Connection conn = null;
                PreparedStatement Statement = null;

                try {
                    conn = SQL.getPoolConnection(SDK.getInstance());
                    Statement = conn.prepareStatement(
                            "INSERT INTO SDK_Preferences SET `plugin`=?, `uuid`=?, `pref`=?, `val`=?;"
                    );

                    Statement.setString(1, caller.getName());
                    Statement.setString(2, player.getUniqueId().toString());
                    Statement.setString(3, key);
                    Statement.setString(4, value);
                    Statement.executeUpdate();

                    SQL.close(SDK.getInstance(), conn);

                } catch (SQLException e) {
                    Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);

                } finally { SQL.close(SDK.getInstance(), Statement, conn); }
            }

        }.runTaskAsynchronously(SDK.getInstance());
    }

    /**
     * Uses UPDATE to update existing preferences into the database
     * @param caller Plugin that is updating the preference
     * @param player Player that the plugin is updating the preference for
     * @param key Key of the preference to be updated
     * @param value Value of the preference that is being updated
     */
    private void updStringPref(Plugin caller, Player player, String key, String value) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Connection conn = null;
                PreparedStatement Statement = null;

                try {
                    conn = SQL.getPoolConnection(SDK.getInstance());
                    Statement = conn.prepareStatement(
                            "UPDATE SDK_Preferences SET `val`=? WHERE `plugin`=? AND `uuid`=? AND `pref`=?;"
                    );

                    Statement.setString(1, value);
                    Statement.setString(2, caller.getName());
                    Statement.setString(3, player.getUniqueId().toString());
                    Statement.setString(4, key);
                    Statement.executeUpdate();

                    SQL.close(SDK.getInstance(), conn);

                } catch (SQLException e) {
                    Logger.Log("Connection to database failed: SQLException", Logit.Level.Error);

                } finally { SQL.close(SDK.getInstance(), Statement, conn); }
            }

        }.runTaskAsynchronously(SDK.getInstance());
    }

    @Override
    public void start() {
        Logger.Log(
            String.format(
                "Received start() signal on %s", this.getClass().getName()

            ), Logit.Level.Info
        );
    }

    @Override
    public void stop() { }

    /**
     * The member preference controller provides a simple
     * interface for plugins to handle basic user preferences
     * without having to create their own database tables.
     * This provides some uniformity in the database to make
     * it easier to share access between the web and the game.
     */
    public MemberPreferenceController() {
        Logger = new Logit(SDK.getInstance());
    }
}
