package com.skynetwork.sdk.Config;

import com.skynetwork.sdk.SDK;

public class DatabaseConfig {
    /**
     * Get MySQL port from the config
     * @return MySQL port
     */
    public String getPort() {
        return SDK.getInstance().getConfig().getString("SQL.Port", "");
    }

    /**
     * Get MySQL host from the config
     * @return MySQL host
     */
    public String getHost() {
        return SDK.getInstance().getConfig().getString("SQL.Host", "");
    }

    /**
     * Get MySQL user from the config
     * @return MySQL user
     */
    public String getUser() {
        return SDK.getInstance().getConfig().getString("SQL.User", "");
    }

    /**
     * Get MySQL password from the config
     * @return MySQL password
     */
    public String getPass() {
        return SDK.getInstance().getConfig().getString("SQL.Pass", "");
    }

    /**
     * Get MySQL database from the config
     * @return MySQL database
     */
    public String getDatabase() {
        return SDK.getInstance().getConfig().getString("SQL.Database", "");
    }

    /**
     * Check if config has valid values
     * @return Config has valid values
     */
    public boolean isValid() {
        return  !getPort().equals("") &&
                !getHost().equals("") &&
                !getUser().equals("") &&
                !getPass().equals("") &&
                !getDatabase().equals("");
    }
}
