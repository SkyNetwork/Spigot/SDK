package com.skynetwork.sdk.Config;

import com.skynetwork.sdk.Debugger.Logit;
import com.skynetwork.sdk.SDK;

public class LoggerConfig {

    /**
     * The level of debug message that will print to
     * the console. Read from SDK configuration file
     * config.yml
     *
     * @return Logit.Level representing the level of log that should be printed to console
     */
    public Logit.Level getPrintLevel() {
        switch(SDK.getInstance().getConfig().getString("Logger.Level", "All")) {
            case "All":
                return Logit.Level.Other; // Other defaults to level 5, showing all
            case "Error":
                return Logit.Level.Error;
            case "Warning":
                return Logit.Level.Warning;
            case "Minimal":
                return Logit.Level.Info;
            default:
                return Logit.Level.Other;
        }
    }

    /**
     * Gets the config value that represents if SDK
     * should shut down a plugin when a critical
     * level exception occurs
     *
     * @return Boolean representing if SDK should shutdown a plugin on critical error
     */
    public boolean getCriticalPlugin() {
        return SDK.getInstance().getConfig().getBoolean("Logger.CriticalShutdown.Plugin", true);
    }

    /**
     * Gets the config value that represents if SDK
     * should shut down the server when a critical
     * level exception occurs
     *
     * @return Boolean representing if SDK should shutdown the server on critical error
     */
    public boolean getCriticalServer() {
        return SDK.getInstance().getConfig().getBoolean("Logger.CriticalShutdown.Server", false);
    }
}
